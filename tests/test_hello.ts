import { hello } from '../src/hello';
import { strict as assert } from 'assert';

describe ('Hello function ', () => {
    it('should return "Hello world!"', () => {
        assert.equal(hello(), "Hello world!");
    });
})